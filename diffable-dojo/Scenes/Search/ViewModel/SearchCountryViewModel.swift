//
//  SearchCountryViewModel.swift
//  diffable-dojo
//
//  Created by Tiago Maia Lopes on 11/03/20.
//  Copyright © 2020 Tiago Maia Lopes. All rights reserved.
//

import Foundation
import UIKit

enum SearchCountrySection {
    case list
}

// MARK: Protocols

protocol SearchCountryViewModelProtocol {

    // MARK: Properties

    var dataSource: UITableViewDiffableDataSource<SearchCountrySection, CountryItemViewModel>? { get set }

    // MARK: Imperatives

    func loadCountries()
    func search(usingQuery query: String)
}

protocol CountryItemViewModelProtocol: Hashable, CustomStringConvertible {

    // MARK: Init

    init(country: Country)
}

// MARK: Implementation

class SearchCountryViewModel: SearchCountryViewModelProtocol {

    // MARK: Properties

    var dataSource: UITableViewDiffableDataSource<SearchCountrySection, CountryItemViewModel>?
    private var countries = [Country]()
    private var countryViewModels = [CountryItemViewModel]() {
        didSet {
            updateList()
        }
    }

    // MARK: Imperatives

    func loadCountries() {
        guard let file = Bundle.main.url(forResource: "countries", withExtension: "json"),
            let data = try? Data(contentsOf: file) else {
                preconditionFailure("The list of countries must be in the bundle.")
        }

        let decoder = JSONDecoder()
        guard let countries = try? decoder.decode([Country].self, from: data) else {
            preconditionFailure("Couldn't parse the countries list.")
        }

        debugPrint("\(countries.count) countries loaded.")

        self.countries = countries
        countryViewModels = countries.map(CountryItemViewModel.init)
    }

    func search(usingQuery query: String) {
        guard !query.isEmpty else {
            countryViewModels = countries.map(CountryItemViewModel.init)
            return
        }

        countryViewModels = countries
            .filter({ $0.name.contains(query) || $0.code.contains(query) })
            .map(CountryItemViewModel.init)
    }

    // MARK: Internal methods

    private func updateList() {
        var snapshot = NSDiffableDataSourceSnapshot<SearchCountrySection, CountryItemViewModel>()
        snapshot.appendSections([.list])
        snapshot.appendItems(countryViewModels, toSection: .list)
        dataSource?.apply(snapshot)
    }
}

struct CountryItemViewModel: CountryItemViewModelProtocol {

    // MARK: Properties

    private let country: Country
    var description: String { "\(country.code), \(country.name)" }

    // MARK: Init

    init(country: Country) {
        self.country = country
    }
}
