//
//  SearchCountryViewController.swift
//  diffable-dojo
//
//  Created by Tiago Maia Lopes on 11/03/20.
//  Copyright © 2020 Tiago Maia Lopes. All rights reserved.
//

import UIKit

class SearchCountryViewController: UIViewController {

    // MARK: Parameters

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    var viewModel: SearchCountryViewModelProtocol!

    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        precondition(viewModel != nil, "The view model must be injected.")

        viewModel.dataSource = UITableViewDiffableDataSource(tableView: tableView, cellProvider: { [unowned self] _, IndexPath, viewModel -> UITableViewCell? in
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "country_cell", for: IndexPath)
            cell.textLabel?.text = viewModel.description
            return cell
        })

        searchBar.searchTextField.addTarget(self, action: #selector(search(_:)), for: .editingChanged)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.loadCountries()
    }

    // MARK: Internal Methods

    @objc private func search(_ textField: UITextField) {
        viewModel.search(usingQuery: textField.text ?? "")
    }
}
