
# Dojo goal

The purpose of this project is to allow hands on practice with the new diffable data source APIs, introduced in the [wwdc19](https://developer.apple.com/videos/play/wwdc2019/220/) event. More specifically, we'll be working with:
- UITableViewDiffableDataSource
- NSDiffableDataSourceSnapshot

### Task:
You'll be finishing the search feature of our app. The app needs to present a simple list of countries to the user, with the possibility for the user to search for specific countries. You **must** use the new DiffableDataSource APIs, otherwise it'd be simple ;).

The current project build is broken =(. fortunately we have you \o/. Please, begin at the **SearchCountryViewModel** file.
