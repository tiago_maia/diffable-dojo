//
//  SceneDelegate.swift
//  diffable-dojo
//
//  Created by Tiago Maia Lopes on 11/03/20.
//  Copyright © 2020 Tiago Maia Lopes. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    // MARK: Properties

    var window: UIWindow?

    // MARK: Delegate Methods

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let _ = (scene as? UIWindowScene) else { return }

        let navigationController = window?.rootViewController as? UINavigationController
        guard let searchController = navigationController?.topViewController as? SearchCountryViewController else {
            preconditionFailure("The search controller must be properly configured.")
        }
        searchController.viewModel = SearchCountryViewModel()
    }
}
