//
//  Country.swift
//  diffable-dojo
//
//  Created by Tiago Maia Lopes on 11/03/20.
//  Copyright © 2020 Tiago Maia Lopes. All rights reserved.
//

import Foundation

struct Country: Codable {

    // MARK: Properties

    let name: String
    let code: String
}

extension Country: Hashable {

}
